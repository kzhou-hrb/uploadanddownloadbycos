package k.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.ServletUtils;

/**
 * Servlet implementation class UDServlet
 */
@WebServlet(urlPatterns = { "/ud" })
public class UDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	//上传下载采用的默认文件编码utf8
	private static final String FILE_CHARSET = "UTF-8";
	
	//文件默认的下载路径
	private static final String DOWNLOAD_PATH = "d:\\test";
	
	//文件默认的上传路径
	private static final String UPLOAD_PATH = "d:\\test";
	//上传文件的大小限制值 5m
	private static final int MAX_POST_SIZE = 5*1024*1024;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UDServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding(FILE_CHARSET);
		response.setCharacterEncoding(FILE_CHARSET);
		String m = request.getParameter("method");
		if("down".equals(m)){
			this.down(request, response);
		}else if("up".equals(m)){
			this.up(request, response);
		}else{
			response.getWriter().println("请求参数错误，仅有[down]/[up]两个参数可选");
		}
	}
	
	private void down(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException{
		String fileName = request.getParameter("fileName");  
		response.setContentType("application/octet-stream");  
		response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, FILE_CHARSET));  
		ServletUtils.returnFile(DOWNLOAD_PATH + "//" + fileName, response.getOutputStream());  
	}
	
	@SuppressWarnings("unchecked")
	private void up(HttpServletRequest request, HttpServletResponse response) throws IOException{
		MultipartRequest multi = new MultipartRequest(request, UPLOAD_PATH, MAX_POST_SIZE,FILE_CHARSET);
		//输出反馈信息
		Enumeration<String> files = multi.getFileNames();
		while (files.hasMoreElements()) {
			String name = files.nextElement();
			File f = multi.getFile(name);
			if(f!=null){
				String fileName = multi.getFilesystemName(name);
				String lastFileName= UPLOAD_PATH+"\\" + fileName;
				response.getWriter().println("上传的文件:"+lastFileName);
			}
		}
	}

}
